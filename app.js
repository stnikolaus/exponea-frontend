$(function () {

    // some language check...
    var userLang = navigator.language || navigator.userLanguage;
    var lg = "en";
    if (userLang == "sk") lg = "sk";


    // register Handlebars Helpers
    Handlebars.registerHelper('ifValue', function(v1, options) {
        if(v1 === pageIndex) {
            return options.fn(this);
        }
        return options.inverse(this);
    });

    $.when(
        // load your language data from a JSON store
        $.ajax({
        dataType: "json",
        url: "lg/" + lg + ".json"
    }),
        // load your external HTML templates
        $.ajax({
            url:"templates/footer.html"
        }),
        $.ajax({
            url:"templates/head.html"
        }),
        $.ajax({
            url:"templates/menu.html"
        }),
        $.ajax({
            url:"templates/index-body.html"
        }),
        $.ajax({
            url:"templates/about-body.html"
        }),
        $.ajax({
            url:"templates/contact-body.html"
        })
    ).done(function(lg,htmlFooter,htmlHeader,htmlMenu,htmlBody,htmlAbout,htmlContact){
            lg[0].head.title = pageTitle;
            lg[0].head.description = pageDescription;

            // render the templates and data, append it to your target in the DOM
            var templateFooter = Handlebars.compile(htmlFooter[0]);
            $(templateFooter(lg[0])).appendTo("footer");

            var templateHeader = Handlebars.compile(htmlHeader[0]);
            $(templateHeader(lg[0])).appendTo("head");

            var theTemplate = Handlebars.compile(htmlMenu[0]);
            $(theTemplate(lg[0])).prependTo("#table-nav-tabs");

            var templateBody = Handlebars.compile(htmlBody[0]);
            $(templateBody(lg[0])).appendTo("#content-body");

            var templateAbout = Handlebars.compile(htmlAbout[0]);
            $(templateAbout(lg[0])).appendTo("#content-body-about");

            var templateContact = Handlebars.compile(htmlContact[0]);
            $(templateContact(lg[0])).appendTo("#content-body-contact");
        }
    );
});